/// Mikayel Ghaziyan
/// 11/10/2017
/// Exercise 2.19

#include <iostream> /// For input and output allowance 

/// Beginning of the main function
int
main()
{   
    /// Declaring Variables for input
    int number1;
    int number2;
    int number3;
    
    /// Obtaining the values
    std::cout << "Please enter three numbers: \n";
    std::cout << "Enter the first number: ";
    std::cin >> number1;
    std::cout << "Enter the second number: ";
    std::cin >> number2;
    std::cout << "Enter the third number: ";
    std::cin >> number3;


    /// Initializing and making the calculation
    int sum = number1 + number2 + number3;
    int avarage = sum / 3;
    int product = number1 * number2 * number3;
     
    /// Evaluating the smalest and the largest numbers
    int min = number1;
    int max = number1;
	 
    if (max < number2) {
       max  = number2;
    }
    if (max < number3)  {
       max = number3;
    }
    if (min > number2)  {
       min = number2;
    }
    if (min > number3)  {
       min = number3;
    }

    /// Displaying the result
     std::cout << "Three input numbers are " << number1 << " " << number2 << " "     << number3 << "\n";
     std::cout << "Sum is " << sum << "\n";
     std::cout << "Avarage is " << avarage << "\n";                                  std::cout << "Product is " << product << "\n";
     std::cout << "Smallest " << min << "\n";
     std::cout << "Largest " << max << "\n";

    return 0; /// Successfull completion 

/// End of the main function
}

/// End of the file

