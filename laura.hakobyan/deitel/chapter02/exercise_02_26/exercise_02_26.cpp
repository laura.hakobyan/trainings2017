/// Display the following checkerboard pattern
#include <iostream>

int
main() 
{  
    std::cout << "Answer1. Display checkerboard with eight output statements: " << std::endl;
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n" << std::endl;
    
    std::cout << "Answer2. Display checkerboard with the few statements as possible: " << std::endl;
    std::cout << "* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * *\n" << std::endl;
 
    return 0;
}

