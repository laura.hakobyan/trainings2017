#include<iostream>

int
main()
{
    int fiveDigitNumber;

    std::cout << "Insert a five digit number: ";
    std::cin >> fiveDigitNumber;

    if (fiveDigitNumber / 100000 != 0) {
        std::cout << "Error 1: is not a five digit number." << std::endl;
        return 1;
    }

    std::cout << fiveDigitNumber / 10000 << "   " 
              << fiveDigitNumber % 10000 / 1000 << "   "
              << fiveDigitNumber % 1000 / 100 << "   " 
              << fiveDigitNumber % 100 / 10 << "   "
              << fiveDigitNumber % 10 << std::endl;         

    return 0;
}          
