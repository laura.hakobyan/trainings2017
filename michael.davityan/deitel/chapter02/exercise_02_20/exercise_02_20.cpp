#include<iostream>

int
main()
{
    int radius;

    std::cout << "Insert the radius of the circle: ";
    std::cin >> radius;

    if (0 > radius) {
        std::cout << "Error 1: wrong radius." << std::endl;
        return 1;
    }

    std::cout << "Length of circle radius is: " << radius << "\n"
              << "Length of circle is: " << (2 * 3.14159 * radius) << "\n"
              << "Square of circle is: " << (3.14159 * radius * radius) << std::endl;

    return 0;
}
