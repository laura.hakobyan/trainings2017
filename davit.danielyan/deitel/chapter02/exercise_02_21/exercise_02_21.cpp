#include <iostream> 

int 
main()
{
    std::cout << "Print figures" << "\n";
    std::cout << "*********" << "\t   ***   " << "\t  *  " << "\t    *    \n";
    std::cout << "*       *" << "\t *     * " << "\t *** " << "\t   * *   \n";
    std::cout << "*       *" << "\t*       *" << "\t*****" << "\t  *   *  \n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t *     * \n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t*       *\n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t *     * \n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t  *   *  \n";
    std::cout << "*       *" << "\t *     * " << "\t  *  " << "\t   * *   \n";
    std::cout << "*********" << "\t   ***   " << "\t  *  " << "\t    *    \n";

    return 0;
}

