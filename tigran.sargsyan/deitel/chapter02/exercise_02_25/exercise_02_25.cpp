#include <iostream>

int
main()
{
    int number1, number2;

    std::cout << "Enter two numbers: ";
    std::cin >> number1 >> number2;

    if (0 == number2) {
        std::cout << "Error 1: the second number is zero." << std::endl;
        return 1;
    }

    if (0 == number1 % number2) {
        std::cout << "The first number is a multiple." << std::endl;
    }

    if (number1 % number2 != 0) {
        std::cout << "The first number is not a multiple." << std::endl;
    }

    return 0;
}

